var myQuestions = [
  {
      question:"A programmer is to a computer as a teacher is to:",
      answers: {
          a: 'A desk',
          b: 'A student',
          c: 'A library'
      },
      correctAnswer: 'b'
  },
  {
      question: "A good algorithm must be",
      answers: {
          a: 'Detailed',
          b: 'Simple',
          c: 'Replaceable'
      },
      correctAnswer: 'a'
  }
];

var quizContainer = document.getElementById('quiz');
var resultsContainer = document.getElementById('results');
var submitButton = document.getElementById('submit');

generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);

function generateQuiz(questions, quizContainer, resultsContainer, submitButton){

  function showQuestions(questions, quizContainer){
     
      var output = [];
      var answers;

      // per secilen pyetje..
      for(var i=0; i<questions.length; i++){
          
                   answers = [];
      
          for(letter in questions[i].answers){
              // buttons
              answers.push(
                  '<label>'
                      + '<input type="radio" name="question'+i+'" value="'+letter+'">'
                      + letter + ': '
                      + questions[i].answers[letter]
                  + '</label>'
              );
          }
          output.push(
              '<div class="question">' + questions[i].question + '</div>'
              + '<div class="answers">' + answers.join('') + '</div>'
          );
      }

      quizContainer.innerHTML = output.join('');
  }

  function showResults(questions, quizContainer, resultsContainer){
   
      var answerContainers = quizContainer.querySelectorAll('.answers');
 
      var userAnswer = '';
      var numCorrect = 0;
      
      for(var i=0; i<questions.length; i++){

            userAnswer = (answerContainers[i].querySelector('input[name=question'+i+']:checked')||{}).value;
              
          if(userAnswer===questions[i].correctAnswer){
          
              numCorrect++;
              
           
              answerContainers[i].style.color = 'lightgreen';
          }
     
          else{
           
              answerContainers[i].style.color = 'red';
          }
      }

      resultsContainer.innerHTML = 'Rezultati : '+ numCorrect + ' prej ' + questions.length;
  }

   showQuestions(questions, quizContainer);
  

  submitButton.onclick = function(){
      showResults(questions, quizContainer, resultsContainer);
  }

}
