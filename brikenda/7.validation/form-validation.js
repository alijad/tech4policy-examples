
    $(document).ready(function(){
       $("#form").validate({
         rules: {
         "firstname":{
             "required": true,
             "minlength": 2,
             "maxlength": 50
          },
         "email":{
             "required": true,
             "email": true,
             "maxlength": 50
          },
        "phoneNumber":{
             "required": true,
             "regex": /^[\+]\d{3} \d{2} \d{3} \d{3}$/,
          },
        "address":{
             "required": true,
          },
        "city":{
             "required": true,
             "maxlength": 15
          },
        "zipCode":{
             "required": true,
             "minlength": 4,
             "maxlength": 5,
             "regex": /^\d+$/
          },
         "website": {
             "required": false,
             "url": true
         }
         "checkbox":{
             "required": true
         }
         "message": {
             "required": true
         }
 
 
         }
       });
     });
