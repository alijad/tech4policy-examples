<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="table.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
  <div class="container">
    <h2>Insert Data</h2>
    <div class="row">
        <div class="col-12">
           <form method="post" >
             <table id="example" class="table table-striped table-bordered mt-4" style="width:100%">
               <thead>
                 <tr>
                   <th>Name</th>
                   <th>Description</th>
                   <th>Image </th>
                   <th>Created_at</th>
                   <th>Insert</th>
                 </tr>
               </thead>
               <tbody id="myTable">
                 <tr >
                   <td><input type='text' name='name' required></td>
                   <td><textarea  name='desc' rows="6" required></textarea></td>
                   <td><input type='text' name='path' required></td>
                   <td><input type='date' name='date' required></td>
                   <td><input type="submit" name="submit" value="Insert"></td>
                 </tr>
               </tbody>
             </table>
           </form>
           <?php
              require 'config.php';

              if (isset($_POST['submit']))
               {
                   $sql="INSERT INTO posts (name,description,image,created_at) VALUES ('".$_POST['name']."','".$_POST['desc']."', '".$_POST['path']."','".$_POST['date']."')";

                   if ($conn->query($sql) === TRUE)
                     {
                       header('Location: index.php');
                     }
                   else
                     {
                       echo "Error: " . $sql . "<br>" . $conn->error;
                     }
               }
           ?>
        </div>
    </div>
  </div>
</body>
</html>