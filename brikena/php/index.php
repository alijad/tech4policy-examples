<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='bootbox/bootbox.min.js'></script>
    <script src="table.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
    <div class="container">
        <h2>Search</h2>

        <input id="myInput" type="text" placeholder="Search..">
        <div class="row">
           <div class="col-12">
                <?php
                        require 'config.php';

                        $result = mysqli_query($conn,"SELECT * FROM posts");




                        echo "<table id=\"example\" class=\"table table-striped table-bordered mt-4\" style=\"width:100%\">         
                                <thead>
                                    <tr>
                                        
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Image </th>
                                        <th>Created_at</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody id=\"myTable\">";


                        while($row = mysqli_fetch_array($result))
                        {

                            echo "<tr >";
                            echo "<td class='text-center align-middle'>" . $row['name'] . "</td>";
                            echo "<td class='text-center align-middle'>" . $row['description'] . "</td>";
                            echo "<td class='text-center align-middle'><img src='images/" . $row['image'] . "' class= \" img-fluid img-thumbnail\" style='width:200px;height:auto;'></td>";
                            echo "<td class='text-center align-middle'>" . $row['created_at'] . "</td>";
                            echo "<td class='text-center align-middle'><a href='edit.php?id=".$row['id']."' ><i style=\"font-size:23px\" class=\"fa\">&#xf044;</i></a> </td>";
                            echo "<td class='text-center align-middle'><a  href='#' class=\"delete\"   id='".$row['id']."'><i class=\"fa fa-trash-o\" style=\"font-size:23px;color:red\"></i></a></td>";
                            echo "</tr>";
                        }

                        echo "</tbody></table>";
                ?>

            <button class="btn btn-primary"><a href='create.php' class="text-white">Create</a></button>

        <!----------  MODAL JQUERY / AJAX FOR DELETE ACTION  ---------->
            <script>
                $(document).ready(function(){

                    // Delete
                    $('.delete').click(function(){
                        var el = this;
                        var id = this.id;


                        // Confirm box
                        bootbox.confirm("Are you sure you want to delete this record?", function(result) {

                            if(result)
                            {
                                // AJAX Request
                                $.ajax
                                ({
                                    url: 'delete.php',
                                    type: 'POST',
                                    data: { id:id },
                                    success: function(response)
                                    {

                                        // Removing row from HTML Table
                                        $(el).closest('tr').fadeOut(800, function()
                                        {
                                            $(this).remove();
                                        });

                                    }
                                });
                            }

                        });

                    });
                });
            </script>
           </div>
        </div>
    </div>
</body>
</html>
