<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="table.js"></script>
</head>
<body>
  <div class="container">
    <h2 class="mb-4">Edit</h2>
      <div class="row">
          <div class="col-12">
              <form method="Post">
                  <?php

                      require 'config.php';

                      $result = mysqli_query($conn,"SELECT * FROM posts where id='".$_GET['id']."'");

                      $id=$_GET['id'];


                      echo "<table id=\"example\" class=\"table table-striped table-bordered mt-4\" style=\"width:100%\">         
                                <thead>
                                    <tr>
                                        
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Image </th>
                                        <th>Created_at</th>
                                        <th>Save</th>
                                    </tr>
                                </thead>
                                <tbody id=\"myTable\">";

                      while($row = mysqli_fetch_array($result))
                        {
                            echo "<tr >";
                            echo "<td><input type='text' name='name' value='" . $row['name'] . "'></td>";
                            echo "<td><textarea id=\"story\" name=\"description\" rows=\"10\" cols=\"33\">" . $row['description']
                                . "</textarea></td>";
                            echo "<td><input type='text' name='path' value='".$row['image'] . "'></td>";
                            echo "<td><input type='date' name='date' value='" . $row['created_at']."'></td>";
                            echo "<td><input type=\"submit\" name=\"submit\" value=\"Update\"></td>";
                            echo "</tr>";
                        }
                      echo "</tbody></table>";
                  ?>

              </form>

              <?php

                  if (isset($_POST['submit']))
                      {
                        $sql= "UPDATE posts SET name='".$_POST['name']."', description='".$_POST['description']."', image='".$_POST['path']."', created_at='".$_POST['date']."'  WHERE id=".$id."";
                        if ($conn->query($sql) === TRUE)
                          {
                            header('Location: index.php');
                          }
                        else
                          {
                            echo "Error updating record: " . $conn->error;
                          }

                        $conn->close();
                      }
              ?>
          </div>
      </div>
  </div>

</body>
</html>

