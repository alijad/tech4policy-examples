    var pyetjet = [
    {
        question: " The brain of any computer system is ",
        answers: {
            a: 'ALU',
            b: 'Memory',
            c: 'CPU',
            d: 'Control unit'
        },
        prgjsakte: 'c'
    },
    {
        question: "A computer program that converts assembly language to machine language is",
        answers: {
            a: 'Compiler',
            b: 'Assembler',
            c: 'Interpreter',
            d: 'Comparator'
        },
        prgjsakte: 'b'
    }
];

var divquiz = document.getElementById('quiz');
var rezultatiishfaqur = document.getElementById('rez');
var submit = document.getElementById('submit');

fillokuizin(pyetjet, divquiz, rezultatiishfaqur, submit);

function fillokuizin(questions, divquiz, rezultatiishfaqur, submit){

    function showQuestions(questions, divquiz){

        var output = []; //rezultati ne dalje
        var answers;  //mundesit e pergjigjeve qe shtohen ne html

        // for each question...
        for(var i=0; i<questions.length; i++){
            answers = [];

            
            for(mundesit in questions[i].answers){
               //per qdo opsion ne json shto nje input type radio dhe nje label per shkrimin 
                answers.push(
                    '<label>'
                        + '<input type="radio" class="mr-3 mb-2" name="question'+i+'" value= "'+mundesit+'">'
                        + mundesit + ': '
                        + questions[i].answers[mundesit]
                    + '</label><br>'
                );
            }
            // radio buttonat se bashku me pytjen
            output.push(
                '<div class="question">' + questions[i].question + '</div>'
                + '<div class="answers ml-3">' + answers.join('') + '</div>'
            );
        }

       
        divquiz.innerHTML = output.join('');
    }


    function showrez(questions, divquiz, rezultatiishfaqur){
        
      //ruajtja e pergjigjjeve dhe shikimi cila eshte 
        var pergjigjet = divquiz.querySelectorAll('.answers');
        var userpergjigjja = '';
        var sakta = 0;
        var pergjigjagabuar=[];
        var f=0;
        var correctans='';
       //kontro cila nga pergjigjet eshte dhene
        for(var i=0; i<questions.length; i++){
            userpergjigjja = (pergjigjet[i].querySelector('input[name=question'+i+']:checked')||{}).value;
             
            if(userpergjigjja===questions[i].prgjsakte){
               
                sakta++;
                
          
            }
          
            else{
               correctans +="\nCorrect answer of question "+ (i+1) +" is "+ questions[i].prgjsakte;
                pergjigjet[i].style.color = 'red';
            }
        }
       
       
        rezultatiishfaqur.innerHTML ="number of correct answer is " + sakta + ' nga ' + questions.length;
         if(sakta==0 || sakta!=questions.length){
            
            alert(correctans);

        }
    }

    
    showQuestions(questions, divquiz);
    submit.onclick = function(){
        showrez(questions, divquiz, rezultatiishfaqur);
    }

}