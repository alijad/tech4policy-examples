<?php

session_start();

$mysqli = new mysqli('localhost', 'root', '', 'blog') or die(mysqli_error($mysqli));

$id = 0;
$update = false;
$name = '';
$description = '';
$image='';

if (isset($_POST['save'])){
    $name = $_POST['name'];
    $description = $_POST['description'];
    $image=$_POST['image'];
    $created_at=date('Y-m-d');

    $mysqli->query("INSERT INTO posts (name, description,image,created_at) VALUES('$name','$description','$image','$created_at')") or
    die($mysqli->error);

    $_SESSION['message'] = "Record has been saved!";
    $_SESSION['msg_type'] = "success";

    header("location: index.php");

}

if (isset($_GET['delete'])){
    $id = $_GET['delete'];
    $mysqli->query("DELETE FROM posts WHERE id=$id") or die($mysqli->error());

    $_SESSION['message'] = "Record has been deleted!";
    $_SESSION['msg_type'] = "danger";

    header("location: index.php");
}

if (isset($_GET['edit'])){
    $id = $_GET['edit'];
    $update = true;
    $result = $mysqli->query("SELECT * FROM posts WHERE id=$id") or die($mysqli->error());
    if (count($result)==1){
        $row = $result->fetch_array();
        $name = $row['name'];
        $description = $row['description'];
        $image=$row['image'];
    }
}

if (isset($_POST['update'])){
    $id = $_POST['id'];
    $name = $_POST['name'];
    $description = $_POST['description'];
    $image=$_POST['image'];

    $mysqli->query("UPDATE posts SET name='$name', description='$description', image='$image' WHERE id=$id") or
    die($mysqli->error);

    $_SESSION['message'] = "Record has been updated!";
    $_SESSION['msg_type'] = "warning";

    header('location: index.php');
}