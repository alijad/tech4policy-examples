
$(document).ready(function() {
  $("form").submit(function (e) {
    
    e.preventDefault();
    
    var name = $('#name').val();
    var zip = $('#zip').val();
    var email = $('#email').val();
    var address = $('#address').val();
    var phone=$('#tel').val();
    var web=$('#web').val();
    var message=$('#message').val();
    var city=$('#city').val();
    
    
    var valid=true;
   
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/i;
    var phoneReg=/^[\+]\d{3} \d{3} \d{3}$/;
    var webReg=/^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/i;
    

    $(".error").remove();

    if(name =='')
    {
    	
       $('#name').after('<span class="error">Name is required!</span>');
       valid=false;
    }
    
    else if(name.length>51)
    {
    
       $('#name').after('<span class="error">Maximum length is 50character</span>');
       valid=false;
    }
    
    if(email=='')
    {

       $('#email').after('<span class="error">Email is required!</span>');
       valid=false;
    }
    
    else if(!emailReg.test(email))
    {
       $('#email').after('<span class="error">Your email is not written correctly. Please write it again!</span>');
       valid=false;
    }
    
    if(address =='')
    {
    	
       $('#address').after('<span class="error">Address is required!</span>');
       valid=false;
    }
    if(zip=='')
    {
       $('#zip').after('<span class="error">Zip code is required!</span>');
       valid=false;
    }
    
    else if(!$.isNumeric(zip))
    {
       $('#zip').after('<span class="error">Zip code should contain only numbers</span>');
       valid=false;
    }
     
    else if(zip.length<4 || zip.length>5)
    {
       $('#zip').after('<span class="error">Zip code length should be 4 or 5</span>');
       valid=false;
    }
    if(city=='')
    {
       $('#city').after('<span class="error">City is required!</span>');
       valid=false;
    }
    else if(city.length>15)
    {
       $('#city').after('<span class="error">City field max length is 15</span>');
       valid=false;
    }
    if(phone=='')
    {
       $('#tel').after('<span class="error">Phone is required!</span>');
       valid=false;
    }
    
    else if(!phoneReg.test(phone))
    {
       $('#tel').after('<span class="error">Your phone is not written correctly. Please write it again!/span>');
       valid=false;
    }
    if(web!="" && !webReg.test(web))
    {
       $('#web').after('<span class="error">Your web is not written correctly. Please write it again!</span>');
       valid=false;
    }
    
    if(!$('#checkbox').is(':checked'))
    {
       $('#terms').after('<span class="error">To continue you should agree with terms and conditions!</span>');
       valid=false;
    }
    if(message=='')
    {
       $('#msg').after('<span class="error">Message is required!</span>');
       valid=false;
    }
    
    else if((message.indexOf('idiot') > -1) || (message.indexOf('crazy') > -1) || (message.indexOf('tayna') > -1))
    {
    	$('#msg').after('<span class="error">Wrong words</span>');
    	$("#message").val("");
       valid=false;
    }
    
   
    if (valid===true)
    {
       $('div#divreg').hide();
       
              $("div#success").css({
    "opacity":"0",
    "display":"block",
}).show().animate({opacity:1});
    }
});

});