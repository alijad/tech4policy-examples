<?php
 echo "Hello World";
?>

<p>This is html content</p>
<?php
echo "After html";
?>

<?php

$someText="<br/>This is string variable";

echo $someText;
?>

<?php
$x = "5";
$y = "4";
echo $x + $y;
?>

<?php

function myTest($name) {
    echo "<p>Name is: $name</p>";
} 
myTest("Driton");

?>

<?php
$x = 5; // global scope

function myTest1() {
	global $x;
    // using x inside this function will generate an error
    echo "<p>Variable x inside function is: $x</p>";
} 
myTest1();

echo "<p>Variable x outside function is: $x</p>";

echo "This ", "string ", "was ", "made ", "with multiple parameters.";
echo "<br/>This ". "string ". "was ". "made ". "with multiple parameters.<br/>";
?>

<?php 
$x = 5985;
var_dump($x);
?>