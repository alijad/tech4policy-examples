$( document ).ready(function() {
	console.log( "ready!" );
});

// Shorthand for $( document ).ready()
$(function() {
	console.log( "ready shorthand!" );
});

$( document ).ready(function() {

	$( "a" ).attr( "href", "http://google.com" );

	$( "a" ).attr({
		title: "all titles are the same too!",
		//href: "http://w3schools.org"
	});

	//alert($("a#showHref").attr('href'));

	//alert($("a#showHref").html());

	$("a#showHref").html("New link");

	$(".content ul li").attr('title','First item of List');
	// Equivalent event setup using the `.on()` method
	$( "p#paragraph" ).on( "click", function() {
	    console.log( "click" );
	});

	$( "p#paragraph" ).hover(function() {
	    console.log( "hover" );
	});

	// Event setup using the `.on()` method with data
		$( "input" ).on(
		    "change",
		    { foo: "bar" }, // Associate data with event binding
		    function( eventObject ) {
		        console.log("An input value has changed! ", eventObject.data.foo);
		    }
		);

		// The hover helper function
	$( ".menu  a" ).hover(function() {
	    $( this ).toggleClass( "hover-menu" );
	});

	//$( "p" ).hide( "slow" );
	$( "div.box" ).click(function() {
  var color = $( this ).css( "background-color" );
  $( "#result" ).html( "That div is <span style='color:" +
    color + ";'>" + color + "</span>." );
});
});