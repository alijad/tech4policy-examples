
$(document).ready(function() {
  $("form").submit(function (e) {
    //stop submit to check for inputs
    e.preventDefault();
    //get value of inputs
     var name = $('#name').val();
    var zip = $('#zip').val();
    var email = $('#email').val();
    var address = $('#address').val();
    var phone=$('#tel').val();
    var web=$('#web').val();
    var message=$('#msg').val();
    var city=$('#city').val();
    
    //to show success
    var valid=true;
    //pattern regex 
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/i;
    var phoneReg=/^[\+]\d{3} \d{3} \d{3}$/;
    var webReg=/^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/i;
    

    $(".error").remove();
     
     //control if name is empty
    if(name =='')
    {
    	//show error
       $('#name').after('<span class="error">Name is required</span>');
       valid=false;
    }
    //control length
    else if(name.length>51)
    {
    	//show error
       $('#name').after('<span class="error">Maximum length is 50character</span>');
       valid=false;
    }
    //check if is empty
    if(email=='')
    {
        //show error
       $('#email').after('<span class="error">Email is required</span>');
       valid=false;
    }
    //check for valid email comparing with regex
    else if(!emailReg.test(email))
    {
       $('#email').after('<span class="error">Please write a valid Email </span>');
       valid=false;
    }
    //check if is empty
    if(address =='')
    {
    	//show error
       $('#address').after('<span class="error">Address is required</span>');
       valid=false;
    }
    if(zip=='')
    {
       $('#zip').after('<span class="error">Zip code is required</span>');
       valid=false;
    }
    //control if is number the value 
    else if(!$.isNumeric(zip))
    {
       $('#zip').after('<span class="error">Zip code should contain numbers</span>');
       valid=false;
    }
    //control length of value 
    else if(zip.length<4 || zip.length>5)
    {
       $('#zip').after('<span class="error">Zip code length should be 4 or 5</span>');
       valid=false;
    }
    if(city=='')
    {
       $('#city').after('<span class="error">City is required</span>');
       valid=false;
    }
    else if(city.length>15)
    {
       $('#city').after('<span class="error">City field max length is 15</span>');
       valid=false;
    }
    if(phone=='')
    {
       $('#tel').after('<span class="error">Phone is required</span>');
       valid=false;
    }
    
    else if(!phoneReg.test(phone))
    {
       $('#tel').after('<span class="error">Please write a valid phone number</span>');
       valid=false;
    }
    if(web!="" && !webReg.test(web))
    {
       $('#web').after('<span class="error">Please write a valid URL</span>');
       valid=false;
    }
    //control if is checked checkbox input
    if(!$('#checkbox').is(':checked'))
    {
       $('#terms').after('<span class="error">You should be agree with terms and conditions to continue</span>');
       valid=false;
    }
    if(message=='')
    {
       $('#msg').after('<span class="error">Message is required</span>');
       valid=false;
    }
    //control words in variable 
    else if((message.indexOf('idiot') > -1) || (message.indexOf('crazy') > -1) || (message.indexOf('tayna') > -1))
    {
    	$('#msg').after('<span class="error">Wrong words</span>');
    	$("#message").val("");
       valid=false;
    }
    
    //if wasnt any error show success message with animation
    if (valid===true)
    {
       $('div#divreg').hide();
       /*$('div#success').css('display','block');*/
       $("div#success").css({
    "opacity":"0",
    "display":"block",
}).show().animate({opacity:1});
    }
});

});