const data=`{
    "Pascal": [
    {
        "Name": "Pascal Made Simple",
        "price": 700
    },
    {
        "Name": "Guide to Pascal",
        "price": 500
    },
    {
        "Name": "Guide to Pascal1",
        "price": 200
    },
    {
        "Name": "Guide to Pascal2",
        "price": 400
    }
    ],
    "Scala": [
    {
        "Name": "Scala for the Impatient",
        "price": 1000
    },
    {
        "Name": "Scala in Depth",
        "price": 1300
    }
    ]
}`;
var container=document.getElementById('content');
var records = JSON.parse(data);

var h1,box,details;
var num=0;

for(var i in records)
{
    h1=document.createElement("h1");
    h1.innerHTML=i;
    container.appendChild(h1);
    for(var j in records[i])
    {
        box=document.createElement('div');
        box.classList.add("box")
        box.innerHTML='<p>'+records[i][j].Name+'</p>';

        details=document.createElement('p');
        details.classList.add("more-info")

        box.appendChild(details);

        function addOnClick(element, price) {
            element.onclick = function () {
                this.querySelector('.more-info').innerHTML="PRICE: "+price;
            };
        }; 
        addOnClick(box,records[i][j].price)
        
        container.appendChild(box);
        num++;
    }

}


